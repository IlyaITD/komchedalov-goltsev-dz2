#include <iostream>
#include <string> 
#include <windows.h>
#define n 8

using namespace std;

const int STOCK_SIZE = 10;
const string currency = " ���. ", thing = "��.";

struct User
{
	string login;
	string password;
};

struct Product
{
	string name;
	int price;
	int num;
};

string change(string item, int b);

int print(Product item[STOCK_SIZE], string value)
{
	bool check = true, wasFound = false;
	int valueCopy = 0, selector = 0;
	valueCopy = atoi(value.c_str());

	if (valueCopy != 0)
	{
		selector = 1;
	}
	else if (value == "all")
	{
		selector = 0;
	}
	else
	{
		selector = 2;
	}

	for (int i = 0; i < STOCK_SIZE; i++) // Information(&Item)[n], ���� ����� ���-�� ������  
	{ // to_string	 
		switch (selector)
		{
		case 0:;
			break;
		case 1: if (item[i].price != valueCopy)
		{
			check = false;
		};
			  break;
		case 2: if (item[i].name != value)
		{
			check = false;
		};

			  break;
		}

		if (item[i].num != 0 && check)
		{
			cout << change(item[i].name, 1);
			cout << change(to_string(item[i].price), 3) + currency;
			cout << change(to_string(item[i].num), 3) + thing;
			cout << endl;
			wasFound = true;
		}
		check = true;
	}
	if (!wasFound)
	{
		cout << "����� � ����� ������ �� ��� ������." << endl;
	}
	cout << endl;
	return selector;
}

string change(string item, int b)
{
	string increment, result;
	int sizeCopy = STOCK_SIZE / b;
	if (item.size() < sizeCopy)
	{
		sizeCopy -= item.size();
		for (int i = 0; i < sizeCopy; i++)
		{
			increment += " ";
		}
	}
	result = item + increment;
	return result;
}

int buying(Product(&item)[STOCK_SIZE], string name, int num)
{
	bool check = false;
	int counter = 0;
	for (int i = 0; i < STOCK_SIZE; i++)
	{
		if (name == item[i].name)
		{
			check = true;
			counter = i;
		}
	}
	if (check)
	{
		item[counter].num -= num;
	}
	return item[counter].num;
}

bool checkingNum(string value, int max)
{
	bool result = true;
	int valueCopy = 0;
	valueCopy = atoi(value.c_str());
	if (valueCopy == 0 && value != "0")
	{
		cout << "�� ����� �����. " << endl;
		result = false;
	}
	if (valueCopy < 1 || valueCopy > max)
	{
		cout << "���������� ������ ����� �� 1 �� " << max << "." << endl;
		result = false;
	}
	else
	{
		;
	}
	return result;
}

int checkingPos(Product(&item)[STOCK_SIZE], string name)
{
	int counter = -1;
	for (int i = 0; i < STOCK_SIZE; i++)
	{
		if (name == item[i].name)
		{
			counter = i;
		}
	}
	if (counter == -1)
	{
		cout << "����� � ����� ������ �� ������. " << endl;
	}
	return counter;
}

int checkingNumOnStock(Product(&item)[STOCK_SIZE], string name)
{
	int counter = -1;
	for (int i = 0; i < STOCK_SIZE; i++)
	{
		if (name == item[i].name)
		{
			counter = i;
		}
	}
	return item[counter].num;
}

bool checkingName(Product(&item)[STOCK_SIZE], string name)
{
	bool check = false, checkNum = false;
	int counter = -1, copy = 0;
	int sizeName = name.size();
	string copyName;
	for (int i = 65; i < 123; i++)
	{
		char counter = char(i);
		for (int j = 0; j < sizeName; j++)
		{
			copyName = name[j];
			if (name[j] == counter)
			{
				check = true;
			}
			if (copy = atoi(copyName.c_str()) != 0 || copyName == "0")
			{
				checkNum = true;
			}
		}
	}
	if (checkNum)
	{
		cout << "�� ����� �����. " << endl;
		return false;
	}
	if (check)
	{
		cout << endl << "�������� ������ ���������� ������� �� ���������. " << endl;
		return false;
	}
	else
	{
		return true;
	}
}

void main()
{
	setlocale(LC_ALL, "Russian");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	Product stock[STOCK_SIZE] =
	{
		"������", 150, 10,
		"������",   18, 4,
		"������",   17, 3,
		"������",   16, 2,
		"������",   20, 1,
		"������",   15, 5,
		"������",   17, 6,
		"������",   10, 7
	};

	//srand(time(NULL));
	//int massif[SIZE_MASSIF][SIZE_MASSIF];
	//int size;
	int cursor = 0, num = 0;
	string name, copy;
	cout << "You can enter 4 for help." << endl << endl;
	do {
		cout << "Enter operation number: ";
		cin >> copy;
		cout << endl << "----------------------------" << endl;
		if (checkingNum(copy, 8))
		{
			cursor = atoi(copy.c_str());
		}
		else
		{
			cursor = 9;
		}

		switch (cursor)
		{
		case 1:  print(stock, "all");
			break;
		case 2:  cout << "Input name or price: ";
			cin >> name;
			if (checkingName(stock, name))
			{
				print(stock, name);
			}
			break;
		case 3: cout << "Input name: ";
			cin.ignore();
			getline(cin, name);
			if (checkingName(stock, name))
			{
				if (checkingPos(stock, name) != -1)
				{
					cout << "Input num: ";
					cin >> copy;
					if (checkingNum(copy, checkingNumOnStock(stock, name)))
					{
						num = atoi(copy.c_str());
						buying(stock, name, num);
					}
					else
					{
						;
					}
				}
			}
			break;
		case 8: cursor = 8;
			break;
		case 9: cursor = 9;
			break;
		}

		cout << "----------------------------" << endl;
		cout << endl;
	} while (cursor != 8);

	//	print(stock, "0"); // ������� ������
	//	print(stock, "������");
		//system("pause");
}

