#include "pch.h"
#include "CppUnitTest.h"
#include "..\Project1\Main.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

Product stock[SIZE] =
{
	"������", 150, 10,
	"�������",   18, 4,
	"���� ����������",   17, 3,
	"����",   16, 2,
	"��������",   20, 1,
	"",   15, 5,
	"",   17, 6,
	"",   10, 7
};

namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			Assert::AreEqual(1, print(stock, "150"));
			Assert::AreEqual(2, print(stock, "������"));
		}

		TEST_METHOD(TestMethod2)
		{
			string str = "   ";
			Assert::AreEqual(str, change(" ", 3));
		}

		TEST_METHOD(TestMethod3)
		{
			Assert::AreEqual(5, buying(stock, "������", 5));
			Assert::AreEqual(2, buying(stock, "������", 3));
		}
	};
}
